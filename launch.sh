#!/bin/sh

set -e

poetry run ./manage.py migrate
poetry run ./manage.py collectstatic --no-input

if [ "$APP_ENVIRONMENT" = "prod" ] || [ "$APP_ENVIRONMENT" = "staging" ]; then
    # launch wsgi app via gunicorn
    exec env DJANGO_SETTINGS_MODULE=ticketlobby.settings gunicorn --workers ${GUNICORN_WORKERS:=2} --bind 0.0.0.0:8000 ticketlobby.wsgi
else
    # launch the django development server
	exec poetry run ./manage.py runserver 0.0.0.0:8000
fi
